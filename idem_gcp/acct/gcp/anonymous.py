try:
    from google.auth.credentials import AnonymousCredentials

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def gather(hub):
    # Break early if an anonymous profile has already been defined in encrypted acct credentials
    for k, v in hub.acct.PROFILES.items():
        if k.startswith("gcp"):
            if "anonymous" in v:
                return {}

    return {"anonymous": {"credentials": AnonymousCredentials()}}
