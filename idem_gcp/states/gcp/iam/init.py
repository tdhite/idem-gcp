# https://cloud.google.com/iam/docs/client-libraries#iam-client-libraries-install-python


def __init__(hub):
    hub.exec.gcp.iam.SERVICE_CACHE = {}


def service(hub, ctx):
    name = repr(ctx.acct.credentials)
    if name not in hub.exec.gcp.iam.SERVICE_CACHE:
        hub.exec.gcp.iam.SERVICE_CACHE[name] = hub.tool.gcp.discovery.build(ctx, "iam")
    return hub.exec.gcp.iam.SERVICE_CACHE[name]
