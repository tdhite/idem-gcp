try:
    import googleapiclient.discovery as discovery

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def _wrap_http_client(hub, target: str):
    def _resource(ctx, *args, **kwargs):
        service = hub.exec.gcp.iam.projects.init.service(ctx)
        with service.serviceAccounts() as service_accounts:
            func = getattr(service_accounts, target)
            request = func(*args, **kwargs)
            return request.execute()

    return _resource


def sub(hub, resource: discovery.Resource):
    """
    Turn a resource from discovery into a sub that can easily be attached to the hub
    """
    hub.pop.sub.add()
