#!/usr/bin/env python3

# Import python libs
import os
import re
import shutil
from setuptools import setup, Command

NAME = "idem_gcp"
DESC = ""

# Version info -- read without importing
_locals = {}
with open(f"{NAME}/version.py") as fp:
    exec(fp.read(), None, _locals)
VERSION = _locals["version"]
SETUP_DIRNAME = os.path.dirname(__file__)
if not SETUP_DIRNAME:
    SETUP_DIRNAME = os.getcwd()

with open("README.rst", encoding="utf-8") as f:
    LONG_DESC = f.read()

with open("requirements.txt") as f:
    REQUIREMENTS = f.read().splitlines()

with open("requirements-extra.txt") as f:
    REQUIREMENTS_EXTRA = {"full": []}
    for req in f:
        match = re.findall(r"google-cloud-(\w+).", req)
        if match:
            REQUIREMENTS_EXTRA[match[0]] = req
            REQUIREMENTS_EXTRA["full"].append(req)


class Clean(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        for subdir in (NAME, "tests"):
            for root, dirs, files in os.walk(
                os.path.join(os.path.dirname(__file__), subdir)
            ):
                for dir_ in dirs:
                    if dir_ == "__pycache__":
                        shutil.rmtree(os.path.join(root, dir_))


def discover_packages():
    modules = []
    for root, _, files in os.walk(os.path.join(SETUP_DIRNAME, NAME)):
        pdir = os.path.relpath(root, SETUP_DIRNAME)
        modname = pdir.replace(os.sep, ".")
        modules.append(modname)
    return modules


setup(
    name=NAME,
    author="",
    author_email="",
    url="",
    version=VERSION,
    install_requires=REQUIREMENTS,
    extras_require=REQUIREMENTS_EXTRA,
    description=DESC,
    long_description=LONG_DESC,
    long_description_content_type="text/x-rst",
    python_requires=">=3.6",
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Development Status :: 5 - Production/Stable",
    ],
    packages=discover_packages(),
    entry_points={},
    cmdclass={"clean": Clean},
)
